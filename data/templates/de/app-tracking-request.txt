Sehr geehrte Damen und Herren,

ich habe Ihre Android-App ({appName}) mal genauer angeschaut und festgestellt, dass Sie darin SDKs einsetzen, die personenbeziehbare IDs an Dritte senden. Beachten Sie, dass die aktuelle Auslegung des § 15 Abs. 3 Satz 1 TMG laut BGH-Urteil (I ZR 7/16) besagt, dass für die Erstellung von Nutzerprofilen "für Zwecke der Werbung oder Marktforschung die Einwilligung des Nutzers erforderlich ist". SDKs, die beispielsweise die Android Advertiser ID nutzen, dürfen nicht ohne Einwilligung aktiv sein.

Folgende[usesingular>n] profilbildende[usesingular>n] Tracker nutzt Ihre App ohne Einwilligung:

{trackerText}

{bghRuling}

[consentedMinimal>Eine Einwilligung zu diesen Datenübertragungen habe ich bei Start oder Installation der App nicht gegeben. Ich habe lediglich eine Minimalauswahl bestätigt, die nur für Bereitstellung des Dienstes absolut notwendige Datenweitergaben genehmigt][selectedConsentButtons> ({selectedConsentButtons})][consentedMinimal>.][notConsented>Eine Einwilligung zu diesen Datenübertragungen habe ich bei Start oder Installation der App nicht gegeben.]

Ich bitte Sie, die SDKs in den nächsten Wochen aus der App zu entfernen bzw. ein funktionierendes Consent-Management einzubauen.
[hasTested>
Bitte beachten, dass ich nach Ablauf von vier Wochen eine Beschwerde bei der zuständigen Behörde einreichen werde, wenn sich bis dahin nichts geändert hat.
]
Herzlichen Dank für Ihr Verständnis,
{addressOfThePersonConcerned}