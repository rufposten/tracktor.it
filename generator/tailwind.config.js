module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html', '../web/**/*.php'],
  theme: {
    extend: {
      colors: {
        primary: '#1d4b7e',
        'primary-dark': '#133357',
        secondary: '#dee5ec',
      },
    },
    fontFamily: {
      display: ["Raleway", "sans-serif"],
      body: ["Raleway", "sans-serif"]
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
