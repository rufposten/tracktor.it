module.exports = {
  devServer: {
    headers: {
      "Access-Control-Allow-Origin": "*"
    }
  },
  eslint: {
    enable: false
  },
  style: {
    postcss: {
      plugins: [require("tailwindcss"), require("autoprefixer")],
    },
  },
};
