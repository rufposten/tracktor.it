export const h3class = "max-w-xl text-xl md:text-2xl font-medium line-break mt-10";
export const inputClass = "font-light my-4 rounded border-2 px-4 py-2 w-full max-w-xl focus:border-primary";
export const inputWithErrorClass = "outline-none border-red-500 text-red-700 focus:border-red-600" + inputClass.replace("focus:border-primary", "");
export const inputErrorMsgClass = "text-red-700 max-w-xl";
export const pClass = "max-w-xl w mb-4 mt-3 text-lg font-light line-break";
