
const header = document.querySelector("body > header");

document.addEventListener("scroll", () => {
    header.classList[(window.scrollY > 0) ? "add" : "remove"]("shadow-md");
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function changeLang (that) {
    setCookie("lang", that.value);
    location.reload();
}

function toggleMenu (that) {
  document.getElementById('menu').classList.toggle('hidden');
  that.querySelector('.close').classList.toggle('hidden');
  that.querySelector('.menu').classList.toggle('hidden');
}
